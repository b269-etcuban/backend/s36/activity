// defines WHEN particular controllers will be used
// Conatin all the endpoints and responses that we can get from controllers

const express = require("express");
// creates a router instance that function as a middleware and routing system
const router = express.Router();

const taskController = require("../controllers/taskController");

// Route to get all the tasks
// http://localhost:3001/tasks
router.get("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});


// route to create taask
router.post("/",(req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

// route to delete task
router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})


// ACTIVITY
router.get("/:id", (req, res) => {
	taskController.getTasks(req.params.id).then(resultFromController => res.send(resultFromController));
})



module.exports = router;