// Contains instructions on how your API will perform its intended tasks
// All the operations it can do will be placed in  this file

const Task = require("../models/task");

// controller function for getting all the tasks
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
};

// controller function for creating a task
module.exports.createTask = (requestBody) => {
	let newTask = new Task ({
		name: requestBody.name
	})
	return newTask.save().then((task, error) => {
		if (error) {
			console.log(error);
			return false;
		} else {
			return task;
		}
	})
}


// controller function for deleting a task
/*
Business Logic
1. Look for the task with the corresponding id provided in the URL/route
2. Delete the task using the Mongoose method "findByIdAndRemove" with the same id provided in the route
*/

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removeTask, err) => {
		if(err) {
			console.log(err);
			return false;
		} else {
			return "Deleted Task";
		}
	})
};

// ACTIVITY
module.exports.getTasks = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result;
	})
};